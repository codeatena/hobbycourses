//
//  ApiService.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/23/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ApiService : NSObject

+ (instancetype) sharedInstance;

typedef void(^RequestCompletionHandler)(NSDictionary *result,NSError *error);
typedef void(^RequestCompletionHandler1)(NSArray *result,NSError *error);

- (void)signUp:(NSString *)username  email:(NSString *)email callback:(RequestCompletionHandler)handler;

@end

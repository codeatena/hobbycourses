//
//  ApiService.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/23/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "ApiService.h"

@implementation ApiService

+ (instancetype) sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (void)signUp:(NSString *)username  email:(NSString *)email callback:(RequestCompletionHandler)handler
{
    
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    NSDictionary *parameters = @{@"username": username ,@"mail" : email};
    [manager POST:@"url" parameters:parameters success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        handler(responseObject ,nil);
        
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        handler(nil , error);
        
    }];
}

@end

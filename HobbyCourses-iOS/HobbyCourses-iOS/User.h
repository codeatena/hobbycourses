//
//  User.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/16/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property (nonatomic, strong)   NSString*       name;
@property (nonatomic, strong)   NSString*       photo;
@property (nonatomic)           BOOL            isOnline;

@end

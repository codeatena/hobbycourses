//
//  Constants.h
//  Busybee
//
//  Created by User on 4/4/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#ifndef Busybee_Constants_h
#define Busybee_Constants_h

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#endif
//
//  SearchViewController.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/17/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchViewController : UIViewController {
    IBOutlet UISwitch* swSearchNear;
    IBOutlet UISwitch* swGuarantee;
    IBOutlet UISwitch* swAccredited;
}

@end

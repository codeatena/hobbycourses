//
//  ShoppingCartViewController.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/15/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface ShoppingCartViewController : UIViewController <SlideNavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray* arrData;
}

@end

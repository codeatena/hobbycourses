//
//  EditProfileViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/20/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "EditProfileViewController.h"
#import "Constants.h"

@interface EditProfileViewController ()

@end

@implementation EditProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidget];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

- (void) initWidget {
    [self hideWidgets];
    [self onMyProfile:nil];
}

- (void) hideWidgets {
    viewMyProfile.hidden = YES;
    viewMycoupons.hidden = YES;
    viewOrderCourses.hidden = YES;
    
    btnMyProfile.backgroundColor = UIColorFromRGB(0xf1f1f1);
    [btnMyProfile setTitleColor:UIColorFromRGB(0x242c39) forState:UIControlStateNormal];
    btnMyCoupons.backgroundColor = UIColorFromRGB(0xf1f1f1);
    [btnMyCoupons setTitleColor:UIColorFromRGB(0x242c39) forState:UIControlStateNormal];
    btnOrderCourses.backgroundColor = UIColorFromRGB(0xf1f1f1);
    [btnOrderCourses setTitleColor:UIColorFromRGB(0x242c39) forState:UIControlStateNormal];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onMyProfile:(id)sender {
    [self hideWidgets];
    viewMyProfile.hidden = NO;
    btnMyProfile.backgroundColor = UIColorFromRGB(0xfe3480);
    [btnMyProfile setTitleColor:UIColorFromRGB(0xf1f1f1) forState:UIControlStateNormal];
}

- (IBAction)onMyCoupons:(id)sender {
    [self hideWidgets];
    viewMycoupons.hidden = NO;
    btnMyCoupons.backgroundColor = UIColorFromRGB(0xfe3480);
    [btnMyCoupons setTitleColor:UIColorFromRGB(0xf1f1f1) forState:UIControlStateNormal];
}

- (IBAction)onOrderCourses:(id)sender {
    [self hideWidgets];
    viewOrderCourses.hidden = NO;
    btnOrderCourses.backgroundColor = UIColorFromRGB(0xfe3480);
    [btnOrderCourses setTitleColor:UIColorFromRGB(0xf1f1f1) forState:UIControlStateNormal];
}

@end

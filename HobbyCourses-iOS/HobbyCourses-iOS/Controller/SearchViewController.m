//
//  SearchViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/17/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "SearchViewController.h"
#import "JTMaterialSwitch.h"
#import "Constants.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidget];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) initWidget {
    JTMaterialSwitch *jswSearchNear = [[JTMaterialSwitch alloc] init];
    jswSearchNear.center = [swSearchNear center];
    swSearchNear.hidden = YES;
    [self initSwitchProperty:jswSearchNear];
    [self.view addSubview:jswSearchNear];
    
    JTMaterialSwitch *jswGuarantee = [[JTMaterialSwitch alloc] init];
    jswGuarantee.center = [swGuarantee center];
    swGuarantee.hidden = YES;
    [self initSwitchProperty:jswGuarantee];
    [self.view addSubview:jswGuarantee];
    
    JTMaterialSwitch *jswAccredited = [[JTMaterialSwitch alloc] init];
    jswAccredited.center = [swAccredited center];
    swAccredited.hidden = YES;
    [self initSwitchProperty:jswAccredited];
    [self.view addSubview:jswAccredited];
}

- (void) initSwitchProperty:(JTMaterialSwitch*) jtSwitch {
    jtSwitch.thumbOnTintColor = UIColorFromRGB(0xfe3480);
    jtSwitch.thumbOffTintColor = UIColorFromRGB(0xa4a4a4);
    jtSwitch.trackOnTintColor = UIColorFromRGB(0xf892b8);
    jtSwitch.trackOffTintColor = UIColorFromRGB(0xcacaca);
}

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onApply:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onClose:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end

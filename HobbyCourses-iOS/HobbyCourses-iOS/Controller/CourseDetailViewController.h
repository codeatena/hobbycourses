//
//  CourseDetailViewController.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/17/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CourseDetailViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>{
    IBOutlet UIImageView*       imvVideoTabSel;
    IBOutlet UIImageView*       imvCertificationTabSel;
    IBOutlet UIImageView*       imvReviewTabSel;
    IBOutlet UIImageView*       imvDurationTabSel;
    
    IBOutlet UITableView*       tblCertifications;
    IBOutlet UITableView*       tblReviews;
    
    NSMutableArray*         arrCertifications;
    NSMutableArray*         arrReviews;
}

@end
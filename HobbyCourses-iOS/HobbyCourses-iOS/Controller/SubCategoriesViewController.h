//
//  SubCategoriesViewController.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/30/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface SubCategoriesViewController : UIViewController <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray* arrData;
    
    IBOutlet UIView* viewCategory;
}

@end

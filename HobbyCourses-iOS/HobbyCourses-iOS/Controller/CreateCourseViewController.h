//
//  CreateCourseViewController.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/18/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface CreateCourseViewController : UIViewController <SlideNavigationControllerDelegate> {
    IBOutlet UISwitch*      swTrialClass;
    IBOutlet UISwitch*      swStatus;
    IBOutlet UISwitch*      swMoneyBack;
    IBOutlet UIView*        viewMain;
    
    IBOutlet UISlider*      sldLeftClassSize;
    IBOutlet UISlider*      sldRightClassSize;
    
    IBOutlet UISlider*      sldLeftAge;
    IBOutlet UISlider*      sldRightAge;
    
    IBOutlet UISlider*      sldSessionNumbers;
    IBOutlet UISlider*      sldSelectTimeDuration;
}

@end

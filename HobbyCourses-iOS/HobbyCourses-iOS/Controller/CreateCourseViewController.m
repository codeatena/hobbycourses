//
//  CreateCourseViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/18/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "CreateCourseViewController.h"
#import "JTMaterialSwitch.h"
#import "Constants.h"
#import "TTRangeSlider.h"
#import "ImageUtility.h"

@interface CreateCourseViewController ()

@end

@implementation CreateCourseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidget];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

- (void) initWidget {
    JTMaterialSwitch *jswTrialClass = [[JTMaterialSwitch alloc] init];
    jswTrialClass.center = [swTrialClass center];
    swTrialClass.hidden = YES;
    [self initSwitchProperty:jswTrialClass];
    [viewMain addSubview:jswTrialClass];
    
    JTMaterialSwitch *jswStatus = [[JTMaterialSwitch alloc] init];
    jswStatus.center = [swStatus center];
    swStatus.hidden = YES;
    [self initSwitchProperty:jswStatus];
    [viewMain addSubview:jswStatus];
    
    JTMaterialSwitch *jswMoneyBack = [[JTMaterialSwitch alloc] init];
    jswMoneyBack.center = [swMoneyBack center];
    swMoneyBack.hidden = YES;
    [self initSwitchProperty:jswMoneyBack];
    [viewMain addSubview:jswMoneyBack];
    
    UIImage* thumbImage = [UIImage imageNamed:@"ellipse.png"];

    [sldLeftClassSize setThumbImage:[ImageUtility imageWithImage:thumbImage scaledToSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
    [sldRightClassSize setThumbImage:[ImageUtility imageWithImage:thumbImage scaledToSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
    [sldLeftAge setThumbImage:[ImageUtility imageWithImage:thumbImage scaledToSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
    [sldRightAge setThumbImage:[ImageUtility imageWithImage:thumbImage scaledToSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
    [sldSessionNumbers setThumbImage:[ImageUtility imageWithImage:thumbImage scaledToSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
    [sldSelectTimeDuration setThumbImage:[ImageUtility imageWithImage:thumbImage scaledToSize:CGSizeMake(10, 10)] forState:UIControlStateNormal];
    

    CGRect trackRect = [sldLeftClassSize trackRectForBounds:sldLeftClassSize.bounds];
    CGRect thumbRect = [sldLeftClassSize thumbRectForBounds:sldLeftClassSize.bounds
                                             trackRect:trackRect
                                                 value:sldLeftClassSize.value];
    
    float left = sldLeftClassSize.frame.origin.x + thumbRect.origin.x + 10;
    
    [sldRightClassSize setTranslatesAutoresizingMaskIntoConstraints:YES];
    CGRect rect = CGRectMake(left, sldLeftClassSize.frame.origin.y - 11, sldRightClassSize.frame.size.width - thumbRect.origin.x - 10, sldLeftClassSize.frame.size.height);
    [sldRightClassSize setFrame:rect];
}

- (void) initSwitchProperty:(JTMaterialSwitch*) jtSwitch {
    jtSwitch.thumbOnTintColor = UIColorFromRGB(0xfe3480);
    jtSwitch.thumbOffTintColor = UIColorFromRGB(0xa4a4a4);
    jtSwitch.trackOnTintColor = UIColorFromRGB(0xf892b8);
    jtSwitch.trackOffTintColor = UIColorFromRGB(0xcacaca);
}

- (IBAction)onSliderLeftClassSize:(id)sender {
    
    CGRect trackRect = [sldLeftClassSize trackRectForBounds:sldLeftClassSize.bounds];
    CGRect thumbRect = [sldLeftClassSize thumbRectForBounds:sldLeftClassSize.bounds
                                                  trackRect:trackRect
                                                      value:sldLeftClassSize.value];
    
    float left = sldLeftClassSize.frame.origin.x + thumbRect.origin.x + 10;
    
    [sldRightClassSize setTranslatesAutoresizingMaskIntoConstraints:YES];
    CGRect rect = CGRectMake(left, sldLeftClassSize.frame.origin.y, sldLeftClassSize.frame.size.width - thumbRect.origin.x - 10, sldLeftClassSize.frame.size.height);
    [sldRightClassSize setFrame:rect];
    
    CGRect newRect = [sldRightClassSize trackRectForBounds:sldRightClassSize.bounds];
    
    float y = newRect.size.width / trackRect.size.width;
    
    sldRightClassSize.value = (y + .7 - 1.0f) / y;
}

@end

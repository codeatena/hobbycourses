//
//  CoursesListViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/13/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "CoursesListViewController.h"
#import "CourseTableViewCell.h"
#import "SampleDataManager.h"

@interface CoursesListViewController ()

@end

@implementation CoursesListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self initWidget];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

- (void) initWidget {
    [self.navigationItem setHidesBackButton:YES];
    self.navigationController.navigationBarHidden = NO;
    viewCategory.hidden = YES;
}

- (void) initData {
    arrData = [[SampleDataManager sharedInstance] getSampleCourses];
}


#pragma mark - tableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    CourseTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CourseTableViewCell"];
    
    Course* course = [arrData objectAtIndex:indexPath.row];
    [cell setData:course];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"course_detail" sender:nil];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 190;
}

#pragma mark - IBActions

- (IBAction)onCategory:(id)sender {
    viewCategory.hidden = !viewCategory.hidden;
}

- (IBAction)onArt:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onBeauty:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onCooking:(id)sender {
    viewCategory.hidden = YES;
}
- (IBAction)onDance:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onEducation:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onFilm:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onFitness:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onLanguages:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onMusic:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onPhotography:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onSport:(id)sender {
    viewCategory.hidden = YES;
}

@end

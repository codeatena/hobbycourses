//
//  ConversationViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/21/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "ConversationViewController.h"
#import "ConvUserTableViewCell.h"
#import "ConvMyMessageTableViewCell.h"
#import "ConvOtherMessageTableViewCell.h"
#import "Constants.h"

@interface ConversationViewController ()

@end

@implementation ConversationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidget];
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    self.title = @"Diane";
    [super viewWillAppear:animated];
}

- (void) initWidget {
    UIColor *color = UIColorFromRGB(0xf1f1f1);
    tfSendMessage.attributedPlaceholder = [[NSAttributedString alloc] initWithString:[tfSendMessage placeholder] attributes:@{NSForegroundColorAttributeName: color}];
}

- (void) initData {
    arrData = [[NSMutableArray alloc] init];
}

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onCamera:(id)sender {
    
}

- (IBAction)onSend:(id)sender {
    [tfSendMessage resignFirstResponder];
}

#pragma mark - tableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {\

    if (indexPath.row == 0) {
        ConvUserTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConvUserTableViewCell"];
        User* user = [[User alloc] init];
        user.name = @"Diane";
        user.isOnline = false;
        user.photo = @"user1.png";
        [cell setData:user];
        return cell;
    }
    if (indexPath.row == 1) {
        ConvMyMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConvMyMessageTableViewCell"];
        Message* message = [[Message alloc] init];
        message.lastTime = @"10m ago";
        message.message = @"Hei Diane, What's new? We are happy cause you like our course! we appreciate people which give us message and let us know what they think.\n Thanks your so much.\nSee your next time!";
        [cell setData:message];
        return cell;
    }
    if (indexPath.row == 2) {
        ConvOtherMessageTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConvOtherMessageTableViewCell"];
        Message* message = [[Message alloc] init];
        message.lastTime = @"1m ago";
        message.message = @"I am fine, thank you! No problems  about review, was a pleasure to participate on your course! I will come back next year for more cool stuff!Have a nice day!";
        [cell setData:message];
        return cell;
    }
    
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (indexPath.row == 0) {
        return 55;
    }
    if (indexPath.row == 1) {
        return 122;
    }
    if (indexPath.row == 2) {
        return 122;
    }
    
    return 0;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

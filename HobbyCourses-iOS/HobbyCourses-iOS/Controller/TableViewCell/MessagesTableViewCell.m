//
//  MessagesTableViewCell.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/16/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "MessagesTableViewCell.h"

@implementation MessagesTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setData:(Message*) message {
    imvUserPhoto.image = [UIImage imageNamed: message.user.photo];
    lblUserName.text = message.user.name;
    lblLastMessage.text = message.message;
    lblLastTime.text = message.lastTime;
    btnStatus.selected = message.user.isOnline;
}

@end

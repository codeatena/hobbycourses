//
//  CourseTableViewCell.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/13/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Course.h"

@interface CourseTableViewCell : UITableViewCell {
    IBOutlet UIImageView*   imvBack;
    IBOutlet UILabel*       lblName;
    IBOutlet UILabel*       lblTitle;
    IBOutlet UILabel*       lblDuration;
    IBOutlet UILabel*       lblPeriod;
    IBOutlet UILabel*       lblAges;
    IBOutlet UILabel*       lblLocation;
    IBOutlet UILabel*       lblNotifyCount;
}

- (void) setData:(Course*) course;

@end

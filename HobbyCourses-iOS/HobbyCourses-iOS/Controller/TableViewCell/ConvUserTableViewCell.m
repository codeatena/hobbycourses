//
//  ConvUserTableViewCell.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/22/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "ConvUserTableViewCell.h"

@implementation ConvUserTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setData: (User*) user {
    imvUserPhoto.image = [UIImage imageNamed: user.photo];
    lblUserName.text = user.name;
    lblLastMessage.text = @"Good course. I really love it!";
    btnStatus.selected = user.isOnline;
}

@end

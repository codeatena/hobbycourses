//
//  ReviewTableViewCell.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/18/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "ReviewTableViewCell.h"
#import "Constants.h"

@implementation ReviewTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setData:(Review*) review {
    imvUser.image = [UIImage imageNamed:review.user.photo];
    lblTitle.text = review.title;
    lblMessage.text = review.text;
    rateView.rating = review.rate;
    [lblTitle sizeToFit];
    rateView.starSize = 10;
    rateView.starNormalColor = [UIColor grayColor];
    rateView.starFillColor = UIColorFromRGB(0xffba00);
//    rateView.center = CGPointMake(lblTitle.center.x + lblTitle.frame.size.width / 2 + 30, rateView.center.y + 5);
}

@end

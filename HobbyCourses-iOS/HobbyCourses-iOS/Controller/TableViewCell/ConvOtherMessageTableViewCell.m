//
//  ConvOtherMessageTableViewCell.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/22/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "ConvOtherMessageTableViewCell.h"

@implementation ConvOtherMessageTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setData: (Message*) message {
    lblTime.text = message.lastTime;
    lblBody.text = message.message;
}

@end

//
//  OrderCourseTableViewCell.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/20/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "OrderCourseTableViewCell.h"

@implementation OrderCourseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setData:(Course*) course {
    lblName.text = course.name;
    lblTitle.text = course.title;
    
    [btnEducation setTitle:[NSString stringWithFormat:@"%d", course.education] forState:UIControlStateNormal];
    [btnUsers setTitle:[NSString stringWithFormat:@"%d", course.user] forState:UIControlStateNormal];
    [btnPost setTitle:course.post forState:UIControlStateNormal];
    [btnAvailable setTitle:course.available forState:UIControlStateNormal];
}

@end

//
//  ReviewTableViewCell.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/18/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Review.h"
#import "RateView.h"

@interface ReviewTableViewCell : UITableViewCell {
    IBOutlet UIImageView*       imvUser;
    IBOutlet UILabel*           lblTitle;
    IBOutlet UILabel*           lblMessage;
    IBOutlet RateView*          rateView;
}

- (void) setData:(Review*) review;

@end

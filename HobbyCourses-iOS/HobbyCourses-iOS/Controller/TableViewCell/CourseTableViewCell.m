//
//  CourseTableViewCell.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/13/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "CourseTableViewCell.h"

@implementation CourseTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) setData:(Course*) course {
    imvBack.image = [UIImage imageNamed:course.imageName];
    lblName.text = course.name;
    lblTitle.text = course.title;
    lblDuration.text = course.duration;
    lblPeriod.text = course.period;
    lblAges.text = course.ages;
    lblLocation.text = course.location;
    lblNotifyCount.text = [NSString stringWithFormat:@"%d", course.notifyCount];
}

@end
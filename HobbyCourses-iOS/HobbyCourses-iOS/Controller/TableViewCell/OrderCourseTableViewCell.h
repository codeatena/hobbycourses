//
//  OrderCourseTableViewCell.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/20/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Course.h"

@interface OrderCourseTableViewCell : UITableViewCell {
    IBOutlet UILabel*   lblName;
    IBOutlet UILabel*   lblTitle;
    IBOutlet UIButton*  btnEducation;
    IBOutlet UIButton*  btnUsers;
    IBOutlet UIButton*  btnPost;
    IBOutlet UIButton*  btnAvailable;
}

- (void) setData:(Course*) course;

@end
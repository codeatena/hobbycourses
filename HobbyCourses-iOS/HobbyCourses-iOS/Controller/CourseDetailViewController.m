//
//  CourseDetailViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/17/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "CourseDetailViewController.h"
#import "SampleDataManager.h"
#import "CertificationTableViewCell.h"
#import "ReviewTableViewCell.h"

@interface CourseDetailViewController ()

@end

@implementation CourseDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidget];
    [self initData];
    [self onCertification:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) initWidget {
    [self hideAllTabs];
}

- (void) hideAllTabs {
    imvVideoTabSel.hidden = YES;
    imvCertificationTabSel.hidden = YES;
    imvReviewTabSel.hidden = YES;
    imvDurationTabSel.hidden = YES;
    
    tblCertifications.hidden = YES;
    tblReviews.hidden = YES;
}

- (void) initData {
    arrCertifications = [[SampleDataManager sharedInstance] getSampleCertifications];
    arrReviews = [[SampleDataManager sharedInstance] getSampleReviews];
}

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onBuy:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSend:(id)sender {
//    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onVideo:(id)sender {
    [self hideAllTabs];
    imvVideoTabSel.hidden = NO;
}

- (IBAction)onCertification:(id)sender {
    [self hideAllTabs];
    imvCertificationTabSel.hidden = NO;
    tblCertifications.hidden = NO;
}

- (IBAction)onReview:(id)sender {
    [self hideAllTabs];
    imvReviewTabSel.hidden = NO;
    tblReviews.hidden = NO;
}

- (IBAction)onDuration:(id)sender {
    [self hideAllTabs];
    imvDurationTabSel.hidden = NO;
}

#pragma mark - tableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == tblCertifications) {
        return [arrCertifications count];
    }
    if (tableView == tblReviews) {
        return [arrReviews count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == tblCertifications) {
        CertificationTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"CertificationTableViewCell"];
        Certification* certification = [arrCertifications objectAtIndex:indexPath.row];
        [cell setData:certification];
        return cell;
    }
    if (tableView == tblReviews) {
        ReviewTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ReviewTableViewCell"];
        Review* review = [arrReviews objectAtIndex:indexPath.row];
        [cell setData:review];
        return cell;
    }
    
    return nil;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == tblCertifications) {
        return 40;
    }
    if (tableView == tblReviews) {
        return 70;
    }
    return 0;
}

@end
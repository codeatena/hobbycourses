//
//  MessagesViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/16/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "MessagesViewController.h"
#import "SampleDataManager.h"
#import "DayMessages.h"
#import "MessagesTableViewCell.h"

@interface MessagesViewController ()

@end

@implementation MessagesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) initData {
    arrData = [[SampleDataManager sharedInstance] getSampleMessages];
}

#pragma mark - tableView Delegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [arrData count];
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    DayMessages* dayMessages = arrData[section];
    return [dayMessages.arrMessages count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MessagesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MessagesTableViewCell"];
    
    DayMessages* dayMessages = arrData[indexPath.section];
    Message* message = [dayMessages.arrMessages objectAtIndex:indexPath.row];
    [cell setData:message];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 55;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 50;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 0;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    UILabel* lblDay = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.view.bounds.size.width, 50.0f)];
    lblDay.textAlignment = NSTextAlignmentCenter;
    DayMessages* dayMessages = arrData[section];
    lblDay.text = dayMessages.day;
    lblDay.textColor = [UIColor grayColor];
    return lblDay;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return [[UIView alloc] initWithFrame:CGRectZero];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self performSegueWithIdentifier:@"conversation" sender:nil];
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

@end

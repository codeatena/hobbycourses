//
//  CoursesListViewController.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/13/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface CoursesListViewController : UIViewController <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray* arrData;
    
    IBOutlet UIView* viewCategory;
}

@end

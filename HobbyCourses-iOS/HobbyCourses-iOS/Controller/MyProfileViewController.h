//
//  MyProfileViewController.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/20/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyProfileViewController : UIViewController {
    IBOutlet UIView*        viewMain;

    IBOutlet UISwitch*      swActivateLocation;
    IBOutlet UISwitch*      swNewsletter;    
}

@end

//
//  SubCategoriesViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/30/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "SubCategoriesViewController.h"

@interface SubCategoriesViewController ()

@end

@implementation SubCategoriesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onCategory:(id)sender {
    viewCategory.hidden = !viewCategory.hidden;
}

- (IBAction)onArt:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onBeauty:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onCooking:(id)sender {
    viewCategory.hidden = YES;
}
- (IBAction)onDance:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onEducation:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onFilm:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onFitness:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onLanguages:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onMusic:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onPhotography:(id)sender {
    viewCategory.hidden = YES;
}

- (IBAction)onSport:(id)sender {
    viewCategory.hidden = YES;
}

@end

//
//  EditProfileViewController.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/20/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface EditProfileViewController : UIViewController <SlideNavigationControllerDelegate> {
    IBOutlet UIView* viewMyProfile;
    IBOutlet UIView* viewMycoupons;
    IBOutlet UIView* viewOrderCourses;
    
    IBOutlet UIButton* btnMyProfile;
    IBOutlet UIButton* btnMyCoupons;
    IBOutlet UIButton* btnOrderCourses;
}

@end

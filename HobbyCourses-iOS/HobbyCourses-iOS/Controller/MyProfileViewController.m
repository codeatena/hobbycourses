//
//  MyProfileViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/20/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "MyProfileViewController.h"
#import "JTMaterialSwitch.h"
#import "Constants.h"

@interface MyProfileViewController ()

@end

@implementation MyProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initWidget];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) initWidget {
    JTMaterialSwitch *jswActivateLocation = [[JTMaterialSwitch alloc] init];
    jswActivateLocation.center = [swActivateLocation center];
    swActivateLocation.hidden = YES;
    [self initSwitchProperty:jswActivateLocation];
    [viewMain addSubview:jswActivateLocation];
    
    JTMaterialSwitch *jswNewsletter = [[JTMaterialSwitch alloc] init];
    jswNewsletter.center = [swNewsletter center];
    swNewsletter.hidden = YES;
    [self initSwitchProperty:jswNewsletter];
    [viewMain addSubview:jswNewsletter];
}

- (void) initSwitchProperty:(JTMaterialSwitch*) jtSwitch {
    jtSwitch.thumbOnTintColor = UIColorFromRGB(0xfe3480);
    jtSwitch.thumbOffTintColor = UIColorFromRGB(0xa4a4a4);
    jtSwitch.trackOnTintColor = UIColorFromRGB(0xf892b8);
    jtSwitch.trackOffTintColor = UIColorFromRGB(0xcacaca);
}

@end

//
//  MyCoursesViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/15/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "MyCoursesViewController.h"
#import "SampleDataManager.h"
#import "MyCoursesTableViewCell.h"
#import "Constants.h"

@interface MyCoursesViewController ()

@end

@implementation MyCoursesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self initData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void) initData {
    arrData = [[SampleDataManager sharedInstance] getSampleCourses];
}

#pragma mark - tableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [arrData count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MyCoursesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCoursesTableViewCell"];
    
    Course* course = [arrData objectAtIndex:indexPath.row];
    [cell setData:course];
    
    cell.rightButtons = @[[MGSwipeButton buttonWithTitle:@"delete" backgroundColor:UIColorFromRGB(0xfe347e) callback:^BOOL(MGSwipeTableCell *sender) {
        NSLog(@"delete button click event!");
        [arrData removeObjectAtIndex:indexPath.row];
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
//        [tableView reloadData];
        return YES;
    }], [MGSwipeButton buttonWithTitle:@"edit" backgroundColor:UIColorFromRGB(0x242c39) callback:^BOOL(MGSwipeTableCell *sender) {
        NSLog(@"edit buttoon click event!");
        return YES;
    }]];
    cell.rightSwipeSettings.transition = MGSwipeTransitionStatic;
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 72;
}

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu {
    return YES;
}

@end

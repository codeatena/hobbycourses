//
//  ConversationViewController.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/21/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ConversationViewController : UIViewController <UITableViewDelegate, UITableViewDataSource> {
    NSMutableArray* arrData;
    IBOutlet UITextField*       tfSendMessage;
}

@end

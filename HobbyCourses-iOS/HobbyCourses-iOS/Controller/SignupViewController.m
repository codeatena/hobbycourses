//
//  SignupViewController.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/12/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "SignupViewController.h"

@interface SignupViewController ()

@end

@implementation SignupViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)onBack:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)onSignup:(id)sender {
    
    NSString* username  = tfUserName.text;
    NSString* email     = tfEmail.text;
    if (username == nil || [username isEqualToString:@""]) {
        
    }
    if (email == nil || [email isEqualToString:@""]) {
        
    }
    [[ApiService sharedInstance] signUp:username email:email callback:^(NSDictionary *result,NSError *error){
        if (error) {
            
        } else {
            
        }
    }];
}

@end

//
//  MessagesViewController.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/16/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SlideNavigationController.h"

@interface MessagesViewController : UIViewController <SlideNavigationControllerDelegate, UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray* arrData;
}

@end

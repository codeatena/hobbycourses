//
//  User.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/16/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "User.h"

@implementation User

@synthesize name, photo, isOnline;

- (id) init {
    self = [super init];
    if (self) {
        photo = @"course_news1.png";
        name = @"";
        isOnline = NO;
    }
    return self;
}

@end
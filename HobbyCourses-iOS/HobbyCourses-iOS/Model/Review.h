//
//  Review.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/17/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface Review : NSObject

@property (nonatomic, strong) User* user;
@property (nonatomic) float rate;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* text;

@end

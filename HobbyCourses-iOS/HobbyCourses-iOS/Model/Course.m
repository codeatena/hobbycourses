//
//  Course.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/13/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "Course.h"

@implementation Course

@synthesize imageName, name, title, duration, period, ages, location, notifyCount;
@synthesize user, post, available, education;

- (id) init {
    self = [super init];
    if (self) {
        imageName = @"course_news1.png";
        name = @"";
        title = @"";
        duration = @"";
        period = @"";
        ages = @"";
        location = @"";
        notifyCount = 0;
        education = 0;
        user = 0;
        post = @"";
        available = @"";
    }
    return self;
}

@end

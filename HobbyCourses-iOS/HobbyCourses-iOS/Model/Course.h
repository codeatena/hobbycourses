//
//  Course.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/13/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Course : NSObject

@property (nonatomic, strong) NSString* imageName;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSString* title;
@property (nonatomic, strong) NSString* duration;
@property (nonatomic, strong) NSString* period;
@property (nonatomic, strong) NSString* ages;
@property (nonatomic, strong) NSString* location;

@property (nonatomic) int education;
@property (nonatomic) int notifyCount;
@property (nonatomic) int user;

@property (nonatomic, strong) NSString* post;
@property (nonatomic, strong) NSString* available;

@end
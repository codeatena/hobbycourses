//
//  Coupon.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/20/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "Coupon.h"

@implementation Coupon

@synthesize title, created, order, coupon, value;

- (id) init {
    self = [super init];
    if (self) {
        title = @"";
        created = @"";
        order = @"";
        coupon = @"";
        value = 30;
    }
    return self;
}

@end
//
//  Message.h
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/16/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "User.h"

@interface Message : NSObject

@property (nonatomic, strong) User* user;
@property (nonatomic, strong) NSString* message;
@property (nonatomic, strong) NSString* lastTime;

@end
//
//  SampleDataManager.h
//  Busybee
//
//  Created by User on 4/12/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SampleDataManager : NSObject

+ (instancetype) sharedInstance;

- (NSMutableArray*) getSampleCourses;
- (NSMutableArray*) getSampleShoppingCarts;
- (NSMutableArray*) getSampleUsers;
- (NSMutableArray*) getSampleMessages;
- (NSMutableArray*) getSampleCertifications;
- (NSMutableArray*) getSampleReviews;
- (NSMutableArray*) getSampleCoupons;
- (NSMutableArray*) getSampleConversation;

@end
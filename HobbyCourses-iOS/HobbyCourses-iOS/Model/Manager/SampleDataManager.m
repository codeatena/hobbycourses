//
//  SampleDataManager.m
//  Busybee
//
//  Created by User on 4/12/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "SampleDataManager.h"
#import "GlobalData.h"
#import "Course.h"
#import "ShoppingCart.h"
#import "Message.h"
#import "DayMessages.h"
#import "Certification.h"
#import "Review.h"
#import "Coupon.h"

@implementation SampleDataManager

+ (instancetype) sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (id) init {
    self = [super init];
    if (self) {

    }
    return self;
}

- (NSMutableArray*) getSampleCourses {
    
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    Course* course = nil;
    
    course = [[Course alloc] init];
    course.imageName = @"course_news1.png";
    course.name = @"Swimming course";
    course.title = @"Swiiming courses for kids beween 3-10 ages.";
    course.duration = @"10:00pm - 6:00am";
    course.period = @"Sun - Sat";
    course.ages = @"3 - 10 ages";
    course.location = @"Mahalaxmi";
    course.notifyCount = 10;
    course.education = 100;

    course.user = 50;
    course.post = @"Posted 3d 15h ago";
    course.available = @"Available until 04-23-2015";
    
    [arrData addObject:course];
    
    course = [[Course alloc] init];
    course.imageName = @"course_news2.png";
    course.name = @"Cooking course";
    course.title = @"Cooking courses for mans beween 20-50 ages.";
    course.duration = @"10:00pm - 2:00am";
    course.period = @"Sun - Sat";
    course.ages = @"20 - 50 ages";
    course.location = @"Burgundi";
    course.notifyCount = 50;
    course.education = 250;
    
    course.user = 40;
    course.post = @"Posted 2d 15h ago";
    course.available = @"Available until 04-24-2015";
    
    [arrData addObject:course];
    
    course = [[Course alloc] init];
    course.imageName = @"course_news3.png";
    course.name = @"Dancing course";
    course.title = @"Dancing courses for everyone beween 5-50 ages.";
    course.duration = @"10:00pm - 10:00am";
    course.period = @"Sun - Sat";
    course.ages = @"5 - 50 ages";
    course.location = @"Delhi";
    course.notifyCount = 150;
    course.education = 200;

    course.user = 30;
    course.post = @"Posted 1d 15h ago";
    course.available = @"Available until 04-25-2015";
    
    [arrData addObject:course];
    
    return arrData;
}

- getSampleShoppingCarts {
    
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    
    ShoppingCart* shoppingCart = nil;
    NSMutableArray* arrCourses = [self getSampleCourses];
    
    shoppingCart = [[ShoppingCart alloc] init];
    shoppingCart.course = arrCourses[0];
    shoppingCart.price = 200;
    shoppingCart.count = 1;
    [arrData addObject:shoppingCart];
    
    shoppingCart = [[ShoppingCart alloc] init];
    shoppingCart.course = arrCourses[1];
    shoppingCart.price = 150;
    shoppingCart.count = 2;
    [arrData addObject:shoppingCart];

    shoppingCart = [[ShoppingCart alloc] init];
    shoppingCart.course = arrCourses[2];
    shoppingCart.price = 250;
    shoppingCart.count = 3;
    [arrData addObject:shoppingCart];

    return arrData;
}

- getSampleUsers {
    
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    
    User* user = nil;
    
    user = [[User alloc] init];
    user.name = @"Diane";
    user.photo = @"user1.png";
    user.isOnline = NO;
    [arrData addObject:user];
    
    user = [[User alloc] init];
    user.name = @"Maria D";
    user.photo = @"user2.png";
    user.isOnline = YES;
    [arrData addObject:user];
    
    user = [[User alloc] init];
    user.name = @"Daniel";
    user.photo = @"user3.png";
    user.isOnline = YES;
    [arrData addObject:user];
    
    user = [[User alloc] init];
    user.name = @"Lucian L. Down";
    user.photo = @"user4.png";
    user.isOnline = NO;
    [arrData addObject:user];
    
    user = [[User alloc] init];
    user.name = @"Lorde";
    user.photo = @"user5.png";
    user.isOnline = NO;
    [arrData addObject:user];
    
    user = [[User alloc] init];
    user.name = @"Nore Lissa G.";
    user.photo = @"user6.png";
    user.isOnline = NO;
    [arrData addObject:user];
    
    return arrData;
}

- getSampleMessages {
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    
    Message* message = nil;
    NSMutableArray* arrUsers = [self getSampleUsers];
    DayMessages* dayMessages = nil;
    
    dayMessages = [[DayMessages alloc] init];
    dayMessages.day = @"TUESDAY, APRIL 7";
    message = [[Message alloc] init];
    message.user = arrUsers[0];
    message.message = @"Good course. I really love it!";
    message.lastTime = @"10m ago";
    [dayMessages.arrMessages addObject:message];
    
    message = [[Message alloc] init];
    message.user = arrUsers[1];
    message.message = @"Your course is awesome! Was a pleasure...";
    message.lastTime = @"1h 10m ago";
    [dayMessages.arrMessages addObject:message];

    message = [[Message alloc] init];
    message.user = arrUsers[2];
    message.message = @"Love to work with your guys!";
    message.lastTime = @"3h ago";
    [dayMessages.arrMessages addObject:message];

    message = [[Message alloc] init];
    message.user = arrUsers[3];
    message.message = @"Good course!!!";
    message.lastTime = @"4h 54m ago";
    [dayMessages.arrMessages addObject:message];
    
    [arrData addObject:dayMessages];
    
    dayMessages = [[DayMessages alloc] init];
    dayMessages.day = @"TUESDAY, MARCH 28";
    message = [[Message alloc] init];
    message.user = arrUsers[4];
    message.message = @"The swimming was stunning! It was unexpecting...";
    message.lastTime = @"10d ago";
    [dayMessages.arrMessages addObject:message];

    message = [[Message alloc] init];
    message.user = arrUsers[5];
    message.message = @"My kid was very very excited after your course...";
    message.lastTime = @"10d ago";
    [dayMessages.arrMessages addObject:message];

    [arrData addObject:dayMessages];
    
    return arrData;
}

- (NSMutableArray*) getSampleCertifications {
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    Certification* cert = nil;
    
    cert = [[Certification alloc] init];
    cert.text = @"Swimming expert. Academy of swimmers Targu-Jiu 2010-2015";
    [arrData addObject:cert];
    
    cert = [[Certification alloc] init];
    cert.text = @"Multi champion until 2009. Master in butterfly swimming style.";
    [arrData addObject:cert];
    
    cert = [[Certification alloc] init];
    cert.text = @"Acreditation from Swimming Expert Group Craiova.";
    [arrData addObject:cert];
    
    cert = [[Certification alloc] init];
    cert.text = @"Kid Swimming legal Tutor.";
    [arrData addObject:cert];
    
    return arrData;
}

- (NSMutableArray*) getSampleReviews {
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    Review* review = nil;
    NSMutableArray* arrUsers = [self getSampleUsers];
    
    review = [[Review alloc] init];
    review.user = arrUsers[0];
    review.rate = 5;
    review.title = @"Very Organised";
    review.text = @"Het appartement van David en June lijkt wel recht uit een woontijdschrft te komenHet appartement van David en June lijkt wel recht uit een";
    [arrData addObject:review];
    
    review = [[Review alloc] init];
    review.user = arrUsers[1];
    review.rate = 4;
    review.title = @"Good Course";
    review.text = @"Het appartement van David en June lijkt wel recht uit een woontijdschrft te komenHet appartement van David en June lijkt wel recht uit een";
    [arrData addObject:review];
    
    review = [[Review alloc] init];
    review.user = arrUsers[2];
    review.rate = 3;
    review.title = @"I love this course";
    review.text = @"Het appartement van David en June lijkt wel recht uit een woontijdschrft te komenHet appartement van David en June lijkt wel recht uit een";
    [arrData addObject:review];
    
    return arrData;
}

- (NSMutableArray*) getSampleCoupons {
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    Coupon* coupon = nil;
    
    coupon = [[Coupon alloc] init];
    coupon.title = @"$30 disscount for Swimming course. New York stadium";
    coupon.created = @"Apr 25,2015";
    coupon.order = @"pending";
    coupon.coupon = @"C34512940DFO1234";
    coupon.value = 30;
    [arrData addObject:coupon];
    
    coupon = [[Coupon alloc] init];
    coupon.title = @"$10 disscount for Swimming course. New York stadium";
    coupon.created = @"Apr 21,2015";
    coupon.order = @"pending";
    coupon.coupon = @"C34512940DFO1233";
    coupon.value = 10;
    [arrData addObject:coupon];
    
    coupon = [[Coupon alloc] init];
    coupon.title = @"$20 disscount for Swimming course. New York stadium";
    coupon.created = @"Apr 22,2015";
    coupon.order = @"pending";
    coupon.coupon = @"C34512940DFO1231";
    coupon.value = 20;
    [arrData addObject:coupon];
    return arrData;
}

- (NSMutableArray*) getSampleConversation {
    NSMutableArray* arrData = [[NSMutableArray alloc] init];
    
    return arrData;
}

@end

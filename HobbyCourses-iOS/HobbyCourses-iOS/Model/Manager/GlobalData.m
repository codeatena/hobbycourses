//
//  GlobalData.m
//  Busybee
//
//  Created by User on 4/12/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import "GlobalData.h"
#import "SampleDataManager.h"

@implementation GlobalData

+ (instancetype) sharedInstance {
    static id instance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        instance = [[self alloc] init];
    });
    return instance;
}

- (id) init {
    self = [super init];
    if (self) {
        
    }
    return self;
}

@end
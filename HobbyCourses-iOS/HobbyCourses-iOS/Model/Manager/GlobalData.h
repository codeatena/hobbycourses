//
//  GlobalData.h
//  Busybee
//
//  Created by User on 4/12/15.
//  Copyright (c) 2015 Luoyan. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GlobalData : NSObject

+ (instancetype) sharedInstance;

@end
//
//  Review.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/17/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "Review.h"

@implementation Review

@synthesize user, rate, title, text;

- (id) init {
    self = [super init];
    if (self) {
        user = [[User alloc] init];
        rate = 5.0f;
        title = @"";
        text = @"";
    }
    return self;
}

@end

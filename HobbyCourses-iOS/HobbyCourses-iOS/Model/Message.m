//
//  Message.m
//  HobbyCourses-iOS
//
//  Created by Code Atena on 11/16/15.
//  Copyright © 2015 Code Atena. All rights reserved.
//

#import "Message.h"

@implementation Message

@synthesize user, message, lastTime;

- (id) init {
    self = [super init];
    if (self) {
        user = [[User alloc] init];
        message = @"";
        lastTime = @"";
    }
    return self;
}

@end